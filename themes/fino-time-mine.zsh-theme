# fino-time.zsh-theme

# Use with a dark background and 256-color terminal!
# Meant for people with RVM and git. Tested only on OS X 10.7.

# You can set your computer name in the ~/.box-name file if you want.

# Borrowing shamelessly from these oh-my-zsh themes:
#   bira
#   robbyrussell
#
# Also borrowing from http://stevelosh.com/blog/2010/02/my-extravagant-zsh-prompt/

function virtualenv_info {
    [ $VIRTUAL_ENV ] && echo '('`basename $VIRTUAL_ENV`') '
}

function prompt_char {
    git branch >/dev/null 2>/dev/null && echo '⠠⠵' && return
    echo '○'
}

function box_name {
  local box="${SHORT_HOST:-$HOST}"
  [[ -f ~/.box-name ]] && box="$(< ~/.box-name)"
  echo "${box:gs/%/%%}"
}

function generate_random_value {
  # color %F<232-256>{$somemessage}%f excluded
  [[ ! -z "$1" && "$1" =~ ^[0-9]+$ ]] && basenum="$1" || basenum=232
  local value=$(shuf -i 0-$((basenum-1)) -n 1)
  echo "${value:0:3}"
}

promptname="NA/N"

# default anon mode on, 0: False; 1: True
anon=1
function switch_anon {
  anon=$((1-anon))
}

zle -N switch_anon{,}
bindkey ^q switch_anon

# print exit code once after last command output
function track-exec-command {
  zsh_exec_command=1
}

function prompt_with_retcode {
  # prepend color code
  local -i lastcode=$?
  if (( lastcode == 0 )); then
    promptname=$(cat /dev/urandom | base64 | tr -dc '0-9a-zA-Z' | head -c4)
    promptname="%{$FG[$(generate_random_value)]%}"$promptname
  elif (( zsh_exec_command != 1 )); then
    promptname=$(cat /dev/urandom | base64 | tr -dc '0-9a-zA-Z' | head -c4)
    promptname="%{$FG[$(generate_random_value)]%}"$promptname
  else
    unset zsh_exec_command
    promptname="✘${(l:3::0:)lastcode}"
    promptname="%{$FG[001]%}"$promptname
  fi
}

# REF: https://stackoverflow.com/questions/59558252/make-zsh-prompt-update-each-time-a-command-is-executed
function randomise_prompt {
  if ((anon)); then
    PROMPT='╭─${promptname}%{$reset_color%} %{$FG[239]%}at%{$reset_color%} %{$FG[033]%}REDACTED%{$reset_color%} %{$FG[239]%}in%{$reset_color%} %{$terminfo[bold]$FG[226]%}REDACTED%{$reset_color%}
╰─$(virtualenv_info)$(prompt_char) '
  else
    PROMPT="╭─%{$FG[040]%}%n%{$reset_color%} %{$FG[239]%}at%{$reset_color%} %{$FG[033]%}$(box_name)%{$reset_color%} %{$FG[239]%}in%{$reset_color%} %{$terminfo[bold]$FG[226]%}%~%{$reset_color%}\$(git_prompt_info)\$(ruby_prompt_info) %D - %*
╰─\$(virtualenv_info)\$(prompt_char) " 
  fi
}

autoload -Uz add-zsh-hook
add-zsh-hook preexec track-exec-command
add-zsh-hook precmd prompt_with_retcode
add-zsh-hook precmd randomise_prompt

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$FG[239]%}on%{$reset_color%} %{$fg[255]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$FG[202]%}✘✘✘"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$FG[040]%}✔"
ZSH_THEME_RUBY_PROMPT_PREFIX=" %{$FG[239]%}using%{$FG[243]%} ‹"
ZSH_THEME_RUBY_PROMPT_SUFFIX="›%{$reset_color%}"
